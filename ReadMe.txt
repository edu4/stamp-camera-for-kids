

===========================================================================
DESCRIPTION:
Stamp camera using AVFoundation

===========================================================================
BUILD REQUIREMENTS:

Xcode 4 or later; iPhone iOS SDK 5.0 or later.

===========================================================================
RUNTIME REQUIREMENTS:

iOS 5.0 or later. This app will not deliver any camera output on the iOS simulator.

===========================================================================
APIs USED:

ALAssetsLibrary - to write to the photos library
AVFoundation
	AVCaptureConnection
	AVCaptureDevice
	AVCaptureDeviceInput
	AVCaptureSession
	AVCaptureStillImageOutput
	AVCaptureVideoDataOutput
	AVCaptureVideoPreviewLayer
        CoreImage

===========================================================================