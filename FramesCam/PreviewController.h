//
//  Created by Eduardo on 11/14/13.
//
//
#import <UIKit/UIKit.h>
#import "StacheCamViewController.h"
#import "TemplateController.h"
#import "CharacterViewController.h"
@protocol PreviewControllerDelegate;
@class CharacterImageView;

@interface PreviewController : UIViewController <UIActionSheetDelegate,UIGestureRecognizerDelegate,StillImageDelegate, TemplateControllerDelegate, CharacterControllerDelegate>


@property (nonatomic, weak) id <PreviewControllerDelegate>      previewControllerDelegate;
@property (nonatomic, weak) IBOutlet UIImageView                *frameImageView;
@property (nonatomic, weak) IBOutlet UIImageView                *characterImageView;
@property (nonatomic, weak) IBOutlet UIImageView                *previewImage;
@property (nonatomic)       BOOL                                isPassingFrameEnabled;
@property (weak, nonatomic) IBOutlet UIView *previewView;

//@property (retain, nonatomic) StacheCamViewController *stacheCamViewController;
@property (strong, nonatomic) CharacterImageView *characterImageView1;


//- (IBAction)backToStacheController:(UIBarButtonItem *)sender;
- (IBAction)clearScreen:(id)sender;
- (IBAction)handlePan:(UIPanGestureRecognizer *)recognizer;
//- (IBAction)savePreview:(id)sender;
- (IBAction)showFrameSelector:(id)sender;
- (IBAction)showCharacterSelector:(id)sender;
- (IBAction)showActionSheet:(id)sender;
- (void)templateControllerLoadFrame:(TemplateController *)sender;
-(void)loadFrame;



@end


