/*
     File: main.m
 Abstract: Application's main entry point
  Version: 1.0
 

 */


#import <UIKit/UIKit.h>

#import "StacheCamAppDelegate.h"

int main(int argc, char *argv[])
{
	//int retVal = 0;
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([StacheCamAppDelegate class]));
	}
	//return retVal;
}
