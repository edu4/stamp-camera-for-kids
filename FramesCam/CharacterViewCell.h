//
//  CharacterViewCell.h
//  FramesCam
//
//  Created by Eduardo on 1/22/14.
//
//

#import <UIKit/UIKit.h>

@interface CharacterViewCell : UICollectionViewCell
@property (nonatomic, weak) IBOutlet UIImageView *characterImageView;
@end
