#import <UIKit/UIKit.h>
#import <CoreMedia/CoreMedia.h>
#import <AVFoundation/AVFoundation.h>
#import "TemplateController.h"
#import "StacheCamAppDelegate.h"



@protocol StillImageDelegate;
@class PreviewController;


@interface StacheCamViewController : UIViewController <UIGestureRecognizerDelegate, AVCaptureVideoDataOutputSampleBufferDelegate, UIAlertViewDelegate,TemplateControllerDelegate>

{
	IBOutlet UISegmentedControl *camerasControl;
	AVCaptureVideoDataOutput    *videoDataOutput;
	dispatch_queue_t            videoDataOutputQueue;
	AVCaptureStillImageOutput   *stillImageOutput;
	UIView                      *flashView;
	BOOL                        isUsingFrontFacingCamera;
	CGFloat                     beginGestureScale;
	CGFloat                     effectiveScale;
}


-(void)     setupAVCapture;
-(IBAction) switchCameras:(id)sender;
-(IBAction) showFrameSelector:(id)sender;


@property(nonatomic, weak)          PreviewController           *previewController;
@property(nonatomic, weak)          StacheCamViewController     *captureManager;
@property(nonatomic, weak)          IBOutlet UIView             *previewView;
@property(nonatomic, weak)          UILabel                     *scanningLabel;

@property(nonatomic, weak)          AVCaptureSession            *captureSession;
@property(nonatomic, retain)        AVCaptureVideoPreviewLayer  *previewLayer;
@property(nonatomic, weak)IBOutlet  UIImageView                 *frameImageView;
@property (weak, nonatomic) IBOutlet UIButton *shutterButton;


//Delegate to load still image in PreviewController
@property(nonatomic,weak)           id                          stillImageDelegate;


@end

@protocol StillImageDelegate <NSObject>

- (void)didSelectStillImage:(NSData *)image withError:(NSError *)error;
- (void)didSelectStillImageWithFrame:(NSData *)image withError:(NSError *)error;

@end

