//
//  TemplateController.h
//
//  Created by Eduardo on 11/18/13.


#import <UIKit/UIKit.h>

@protocol TemplateControllerDelegate;

@interface TemplateController : UIViewController
{
    //ivars go here.
}

//Making public TemplateControllerDelegate
@property(nonatomic, weak) id <TemplateControllerDelegate>frameDelegate;
@property(nonatomic, weak)   NSMutableArray *selectedFrames;
@property(nonatomic, weak)  NSString *selectedLabelStr;
@property(nonatomic, strong) NSArray *frameImages;//cannot use weak
@property(nonatomic, weak)  IBOutlet UICollectionView *collectionView;


- (IBAction)back:(id)sender;
@end

@protocol TemplateControllerDelegate <NSObject>

- (void)templateControllerLoadFrame:(TemplateController *)sender;

@end
