//
//  Created by Eduardo on 11/26/13.


#import <Foundation/Foundation.h>


@interface DataController : NSObject
{
    //Used to load the frame
    NSString *result;
}

@property (nonatomic, strong) NSString *result;

+(DataController*)sharedInstance;
+ (void)resetSharedInstance;


@end
