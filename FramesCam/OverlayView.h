//
//  OverlayView.h
//  OverlayViewTester
//
//  Created by Eduardo on 11/18/13.


#import <UIKit/UIKit.h>
#import "StacheCamViewController.h"


@interface OverlayView : UIViewController {
    
}


@property (retain) StacheCamViewController *captureManager;
@property (nonatomic, retain) UILabel *scanningLabel;

@end

