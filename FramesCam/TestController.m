//
//  TestController.m
//  FramesCam
//
//  Created by Eduardo on 11/21/13.
//
//

#import "TestController.h"
#import "TemplateController.h"

@implementation TestController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	self.frameImageView.image = [UIImage imageNamed:self.passFrameLabel];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)close:(id)sender {
    [self dismissViewControllerAnimated:YES completion:NULL];
}
- (void)dealloc {
    [_frameImageView release];
    [super dealloc];
}
@end
