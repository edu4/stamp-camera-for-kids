#import <UIKit/UIKit.h>

@class StacheCamViewController;
@interface StacheCamAppDelegate : NSObject <UIApplicationDelegate>


@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) StacheCamViewController *stacheCamViewController;
@end


