#import "StacheCamViewController.h"
#import <CoreImage/CoreImage.h>
#import <ImageIO/ImageIO.h>
#import <AssertMacros.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "PreviewController.h"
#import "DataController.h"
#import <AudioToolbox/AudioToolbox.h>
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


static const NSString *AVCaptureStillImageIsCapturingStillImageContext = @"AVCaptureStillImageIsCapturingStillImageContext";
static CGFloat DegreesToRadians(CGFloat degrees)
    {
        return degrees * M_PI / 180;
    };

@interface UIImage (RotationMethods)
@end

@implementation UIImage (RotationMethods)

- (UIImage *)imageRotatedByDegrees:(CGFloat)degrees 
    {
        // calculate the size of the rotated view's containing box for our drawing space
        UIView *rotatedViewBox = [[UIView alloc] initWithFrame:CGRectMake(0,0,self.size.width, self.size.height)];
        CGAffineTransform t = CGAffineTransformMakeRotation(DegreesToRadians(degrees));
        rotatedViewBox.transform = t;
        CGSize rotatedSize = rotatedViewBox.frame.size;
	
        // Create the bitmap context
        UIGraphicsBeginImageContext(rotatedSize);
        CGContextRef bitmap = UIGraphicsGetCurrentContext();
	
        // Move the origin to the middle of the image so we will rotate and scale around the center.
        CGContextTranslateCTM(bitmap, rotatedSize.width/2, rotatedSize.height/2);
	
        // Rotate the image context
        CGContextRotateCTM(bitmap, DegreesToRadians(degrees));
	
        // Now, draw the rotated/scaled image into the context
        CGContextScaleCTM(bitmap, 1.0, -1.0);
        CGContextDrawImage(bitmap, CGRectMake(-self.size.width / 2, -self.size.height / 2, self.size.width, self.size.height), [self CGImage]);
	
        UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return newImage;
	
    }

@end

@interface StacheCamViewController (_InternalMethods)
- (void)setupAVCapture;
- (void)teardownAVCapture;

- (void)drawFaceBoxesForFeatures:(NSArray *)features forVideoBox:(CGRect)clap orientation:(UIDeviceOrientation)orientation;
@end

@implementation StacheCamViewController
{
    UIImageView *overlayImageView;
    UIAlertView *alert;
}

@synthesize stillImageDelegate,captureSession,frameImageView,previewLayer, captureManager, previewView,shutterButton;

- (void)setupAVCapture
{
	NSError *error = nil;
	
	AVCaptureSession *session = [AVCaptureSession new];
	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
        
	    [session setSessionPreset:AVCaptureSessionPresetPhoto];
	else
	    [session setSessionPreset:AVCaptureSessionPresetPhoto];
	
	AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
	AVCaptureDeviceInput *deviceInput = [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];
	//require( error == nil, bail );
	isUsingFrontFacingCamera = NO;
	if ( [session canAddInput:deviceInput] )
		[session addInput:deviceInput];
	// creating object for still image
	stillImageOutput = [AVCaptureStillImageOutput new];
    
	[stillImageOutput addObserver:self forKeyPath:@"capturingStillImage" options:NSKeyValueObservingOptionNew context:(__bridge void *)(AVCaptureStillImageIsCapturingStillImageContext)];
	if ( [session canAddOutput:stillImageOutput] )
		[session addOutput:stillImageOutput];
	effectiveScale = 1.0;
	previewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:session];
	[previewLayer setBackgroundColor:[[UIColor blackColor] CGColor]];
    //Fill the whole UIView
	[previewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    UIView *view = [self previewView];
    CALayer *viewLayer = [view layer];
    [viewLayer setMasksToBounds:YES];
    
    CGRect bounds = [view bounds];
    [previewLayer setFrame:bounds];
	CALayer *rootLayer = [previewView layer];
	[rootLayer setMasksToBounds:YES];
    [previewLayer setFrame:[rootLayer bounds]];
	//[previewLayer setFrame:[rootLayer bounds]];
	[rootLayer addSublayer:previewLayer];
	[session startRunning];
    bail:
	;
    
	if (error)
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:
            [NSString stringWithFormat:@"Failed with error %d", (int)[error code]]
            message:[error localizedDescription] delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil];
            [alertView show];
            [self teardownAVCapture];
        }
}
					
- (void)teardownAVCapture
    {
        [stillImageOutput removeObserver:self forKeyPath:@"isCapturingStillImage"];
        [previewLayer removeFromSuperlayer];
    }

// Capture Still Image
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	if ( context == (__bridge void *)(AVCaptureStillImageIsCapturingStillImageContext) )
    {
		BOOL isCapturingStillImage = [[change objectForKey:NSKeyValueChangeNewKey] boolValue];
		
		if ( isCapturingStillImage )
            {
                // Flash animation
                flashView = [[UIView alloc] initWithFrame:[previewView frame]];
                [flashView setBackgroundColor:[UIColor whiteColor]];
                [flashView setAlpha:1.0f];
                [[[self view] window] addSubview:flashView];
                [UIView animateWithDuration:.4f animations:^
                    {
                        [flashView setAlpha:1.f];
                    }
                 ];
            }
		else
            {
                [UIView animateWithDuration:.4f animations:^
                    {
                        [flashView setAlpha:0.f];
                    }
                completion:^(BOOL finished)
                    {
                        [flashView removeFromSuperview];
                        flashView = nil;
                    }
                 ];
            }
	}
}

- (AVCaptureVideoOrientation)avOrientationForDeviceOrientation:(UIDeviceOrientation)deviceOrientation
{
	AVCaptureVideoOrientation result = (AVCaptureVideoOrientation)deviceOrientation;
	if ( deviceOrientation == UIDeviceOrientationLandscapeLeft )
		result = AVCaptureVideoOrientationLandscapeRight;
	else if ( deviceOrientation == UIDeviceOrientationLandscapeRight )
		result = AVCaptureVideoOrientationLandscapeLeft;
	return result;
}


- (void)displayErrorOnMainQueue:(NSError *)error withMessage:(NSString *)message
{
	dispatch_async(dispatch_get_main_queue(), ^(void)
    {
		UIAlertView *alertView = [[UIAlertView alloc]
        initWithTitle:[NSString stringWithFormat:@"%@ (%d)", message, (int)[error code]]
		message:[error localizedDescription] delegate:nil  cancelButtonTitle:@"Dismiss" otherButtonTitles:nil];
		[alertView show];
	});
}

- (void)takePicture
{
    // Find out the current orientation and tell the still image output.
    AVCaptureConnection *stillImageConnection = [stillImageOutput connectionWithMediaType:AVMediaTypeVideo];    
    UIDeviceOrientation curDeviceOrientation = [[UIDevice currentDevice] orientation];
    AVCaptureVideoOrientation avcaptureOrientation = [self avOrientationForDeviceOrientation:curDeviceOrientation];
    
    [stillImageConnection setVideoOrientation:avcaptureOrientation];
    [stillImageConnection setVideoScaleAndCropFactor:effectiveScale];
    [stillImageOutput setOutputSettings:[NSDictionary dictionaryWithObject:AVVideoCodecJPEG
            forKey:AVVideoCodecKey]];
    alert = [[UIAlertView alloc] initWithTitle: @"Loading..." message: nil delegate: nil cancelButtonTitle:nil otherButtonTitles:nil];
    [alert show];
    [self timedAlert];
        [stillImageOutput captureStillImageAsynchronouslyFromConnection:stillImageConnection
        completionHandler:^(CMSampleBufferRef imageDataSampleBuffer, NSError *error)//
         
    {
       
        if (error)
            {
                [self displayErrorOnMainQueue:error withMessage:@"Take picture failed"];
            }
        else
            {
                // Simple JPEG case
                NSData *jpegData = [AVCaptureStillImageOutput
                jpegStillImageNSDataRepresentation:imageDataSampleBuffer];
                
                if (overlayImageView.image == nil)
                {
                    if
                        ([self.stillImageDelegate respondsToSelector:@selector(didSelectStillImage:withError:)])
                    {
                        [self.stillImageDelegate didSelectStillImage:jpegData  withError:error];
                        
                    }
                    else
                    {
                        NSLog(@"Delegate did not respond to message");
                        
                    }
                }
                else
                {
                    if
                        ([self.stillImageDelegate respondsToSelector:@selector(didSelectStillImageWithFrame:withError:)])
                    {
                        [self.stillImageDelegate didSelectStillImageWithFrame:jpegData  withError:error];
                        
                    }
                    else
                    {
                        NSLog(@"Delegate did not respond to message");
                        
                    }


                }
                    
                
             
            }
    }
         ];
    
}

-(void)timedAlert
{
    [self performSelector:@selector(dismissAlert:) withObject:alert afterDelay:1];
}
-(void)dismissAlert:(UIAlertView *)alertView
{
    [alertView dismissWithClickedButtonIndex:0 animated:YES];
    
}


- (IBAction)showFrameSelector:(id)sender
{
    if([UIScreen mainScreen].bounds.size.height >= 568)
    {
    UIStoryboard *storyboard;
    storyboard = [UIStoryboard storyboardWithName:@"iphone5" bundle:nil];
    TemplateController *templateController = [storyboard instantiateViewControllerWithIdentifier:@"TemplateController"];
    templateController.frameDelegate = self;
    [self presentViewController:templateController animated:YES completion:nil];
    }
    else
    {
        UIStoryboard *storyboard;
        storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil];
        TemplateController *templateController = [storyboard instantiateViewControllerWithIdentifier:@"TemplateController"];
        templateController.frameDelegate = self;
        [self presentViewController:templateController animated:YES completion:nil];
        
    }
}





- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection
{	
	// Got an image.
	CFDictionaryRef attachments = CMCopyDictionaryOfAttachments(kCFAllocatorDefault, sampleBuffer, kCMAttachmentMode_ShouldPropagate);
	if (attachments)
		CFRelease(attachments);
	NSDictionary *imageOptions = nil;
	UIDeviceOrientation curDeviceOrientation = [[UIDevice currentDevice] orientation];
	int exifOrientation;
	
	enum
    {
		PHOTOS_EXIF_0ROW_TOP_0COL_LEFT			= 1, //   1  =  0th row is at the top, and 0th column is on the left (THE DEFAULT).
		PHOTOS_EXIF_0ROW_TOP_0COL_RIGHT			= 2, //   2  =  0th row is at the top, and 0th column is on the right.  
		PHOTOS_EXIF_0ROW_BOTTOM_0COL_RIGHT      = 3, //   3  =  0th row is at the bottom, and 0th column is on the right.  
		PHOTOS_EXIF_0ROW_BOTTOM_0COL_LEFT       = 4, //   4  =  0th row is at the bottom, and 0th column is on the left.  
		PHOTOS_EXIF_0ROW_LEFT_0COL_TOP          = 5, //   5  =  0th row is on the left, and 0th column is the top.  
		PHOTOS_EXIF_0ROW_RIGHT_0COL_TOP         = 6, //   6  =  0th row is on the right, and 0th column is the top.  
		PHOTOS_EXIF_0ROW_RIGHT_0COL_BOTTOM      = 7, //   7  =  0th row is on the right, and 0th column is the bottom.  
		PHOTOS_EXIF_0ROW_LEFT_0COL_BOTTOM       = 8  //   8  =  0th row is on the left, and 0th column is the bottom.  
	};
	
	switch (curDeviceOrientation) {
		case UIDeviceOrientationPortraitUpsideDown:  // Device oriented vertically, home button on the top
			exifOrientation = PHOTOS_EXIF_0ROW_LEFT_0COL_BOTTOM;
			break;
		case UIDeviceOrientationLandscapeLeft:       // Device oriented horizontally, home button on the right
			if (isUsingFrontFacingCamera)
				exifOrientation = PHOTOS_EXIF_0ROW_BOTTOM_0COL_RIGHT;
			else
				exifOrientation = PHOTOS_EXIF_0ROW_TOP_0COL_LEFT;
			break;
		case UIDeviceOrientationLandscapeRight:      // Device oriented horizontally, home button on the left
			if (isUsingFrontFacingCamera)
				exifOrientation = PHOTOS_EXIF_0ROW_TOP_0COL_LEFT;
			else
				exifOrientation = PHOTOS_EXIF_0ROW_BOTTOM_0COL_RIGHT;
			break;
		case UIDeviceOrientationPortrait:            // Device oriented vertically, home button on the bottom
		default:
			exifOrientation = PHOTOS_EXIF_0ROW_RIGHT_0COL_TOP;
			break;
	}

	imageOptions = [NSDictionary dictionaryWithObject:[NSNumber numberWithInt:exifOrientation] forKey:CIDetectorImageOrientation];
	
}

- (void)dealloc
{
	[self teardownAVCapture];;

    [[self captureSession] stopRunning];
	previewLayer = nil;
	captureSession = nil;
}

- (IBAction)switchCameras:(id)sender
{
	AVCaptureDevicePosition desiredPosition;
	if (isUsingFrontFacingCamera)
		desiredPosition = AVCaptureDevicePositionBack;
	else
		desiredPosition = AVCaptureDevicePositionFront;
	
	for (AVCaptureDevice *d in [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo])
    {
		if ([d position] == desiredPosition) {
			[[previewLayer session] beginConfiguration];
			AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:d error:nil];
			for (AVCaptureInput *oldInput in [[previewLayer session] inputs]) {
				[[previewLayer session] removeInput:oldInput];
			}
			[[previewLayer session] addInput:input];
			[[previewLayer session] commitConfiguration];
			break;
		}
	}
	isUsingFrontFacingCamera = !isUsingFrontFacingCamera;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
    {
        NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                        [UIColor whiteColor],NSForegroundColorAttributeName,
                                        [UIColor whiteColor],NSBackgroundColorAttributeName,nil];
        
        self.navigationController.navigationBar.titleTextAttributes = textAttributes;
        self.title = @"Camera";
        [self.view setBackgroundColor:[UIColor colorWithRed:207.0/255.0
                                                      green:16.0/255.0
                                                       blue:13.0/255.0
                                                      alpha:1.0]];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [super viewDidLoad];
	[self setupAVCapture];
    }


- (void)viewDidUnload
    {
        [super viewDidUnload];
    }

- (void)viewWillAppear:(BOOL)animated
    {
        [self loadFrame];
        [shutterButton setFrame:CGRectMake(125, (self.view.bounds.size.height-130), 80,75)];
        [[self view] addSubview:shutterButton];
        UIBarButtonItem *cameraItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCamera target:self action:nil];
        NSArray *actionButtonItems = @[cameraItem];
        self.toolbarItems = actionButtonItems;
        [super viewWillAppear:animated];
    }


- (void)viewDidAppear:(BOOL)animated
    {
        [super viewDidAppear:animated];
    }

- (void)viewWillDisappear:(BOOL)animated
    {
        [super viewWillDisappear:animated];
    }

- (void)viewDidDisappear:(BOOL)animated
    {
        [super viewDidDisappear:animated];
    }

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
    {
        // Return YES for supported orientations
        return (interfaceOrientation == UIInterfaceOrientationPortrait);
    }

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
    {
        if ( [gestureRecognizer isKindOfClass:[UIPinchGestureRecognizer class]] )
            {
                beginGestureScale = effectiveScale;
            }
        return YES;
    }


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
    {
        TemplateController *viewTemplateController = (TemplateController *)segue.destinationViewController;
        self.stillImageDelegate = viewTemplateController;
    
        if([segue.identifier isEqualToString:@"pushPreviewImage"])
            {
                
                // Set the delegate so this controller can received snapped photos
                PreviewController *viewController = (PreviewController *)segue.destinationViewController;
                self.stillImageDelegate = viewController;
                [self takePicture];
            }

    }

-(void)loadFrame;
    {
        [DataController sharedInstance];
        [overlayImageView removeFromSuperview];
        DataController *dataController = [DataController sharedInstance];
        overlayImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:dataController.result]];
        [overlayImageView setFrame:CGRectMake(0, 64, self.view.bounds.size.width,(self.view.bounds.size.height-110))];
        [[self view] addSubview:overlayImageView];
        [[captureManager captureSession] startRunning];
        NSLog(@"%@",dataController);
        
    }
- (void)templateControllerLoadFrame:(TemplateController *)sender
    {
      [self loadFrame];
    }

@end
