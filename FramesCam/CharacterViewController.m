//
//  CharacterControllerViewController.m
//  FramesCam
//
//  Created by Eduardo on 1/22/14.
//
//

#import "CharacterViewController.h"
#import "CharacterViewCell.h"
//#import "DataController.h"

@interface CharacterViewController ()<UITextFieldDelegate, UICollectionViewDelegateFlowLayout>

@end

@implementation CharacterViewController
@synthesize characterImages;

- (void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.topItem.title = @"";
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Initialize image array
    NSArray *framesArray = [NSArray arrayWithObjects:@"PatrolCar1.png", @"PatrolCar2.png", @"Ambulance1.png", @"Ambulance2.png", @"Ambulance3.png", @"Ambulance4.png", @"Train1.png",@"Train2.png",@"Train3.png", @"Train4.png",@"Train5.png",@"Train6.png",@"Train7.png",@"Train8.png",@"Train9.png",@"Train10.png",nil];
    self.characterImages = [NSArray arrayWithObjects: framesArray, nil];
    UICollectionViewFlowLayout *collectionViewLayout = (UICollectionViewFlowLayout*)self.collectionView.collectionViewLayout;
    collectionViewLayout.sectionInset = UIEdgeInsetsMake(20, 0, 40, 40);
    _selectedCharacter = [NSMutableArray array];
   
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    //Dispose of any resources that can be recreated.
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return [self.characterImages count];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [[self.characterImages objectAtIndex:section] count];
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"Cell";
    CharacterViewCell *cell = (CharacterViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    CharacterViewCell *characterImageViewCell = (CharacterViewCell *)[cell viewWithTag:101];
    characterImageViewCell.characterImageView.image = [UIImage imageNamed:[self.characterImages[indexPath.section] objectAtIndex:indexPath.row]];
    
    return cell;
   
    
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    _selectedLabelStr = [self.characterImages[indexPath.section] objectAtIndex:indexPath.row];
    [self.collectionView deselectItemAtIndexPath:indexPath animated:NO];
  //  DataController *myStore = [DataController sharedInstance];
  //  myStore.result = _selectedLabelStr;
    
    [self dismissViewControllerAnimated:YES completion:
     ^{
         if ([self.characterDelegate respondsToSelector:@selector(characterControllerLoad:)])
         {
             
             [self.characterDelegate performSelector:@selector(characterControllerLoad:) withObject:self];
         }
     }];
}



- (IBAction)back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:
     
     
     ^{
        
         if ([self.characterDelegate respondsToSelector:@selector(viewWillAppear:)])
         {
             
             [self.characterDelegate performSelector:@selector(viewWillAppear:) withObject:self];
         }
     }];
}

@end
