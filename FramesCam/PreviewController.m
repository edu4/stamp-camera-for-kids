//
//  Created by Eduardo on 11/14/13.
//
//
#import <Social/Social.h>
#import "StacheCamViewController.h"
#import "PreviewController.h"
#import "DataController.h"
#import "CharacterViewController.h"

@interface PreviewController ()
{
    CGFloat lastScale;
    CGFloat pinchScale;
    CGFloat maximumHeight;
    CGFloat minimumHeight;
    UITapGestureRecognizer *singleTap;
    UIImageView *tmpCharacterUIIV;
    UIImage *tmpCharacterUImage;
    UIImage *mergedImage;
}

@end

@implementation PreviewController

@synthesize frameImageView,characterImageView;

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

- (void)pasteImage:(UITapGestureRecognizer *)sender
{
    tmpCharacterUIIV = [[UIImageView alloc]initWithImage:tmpCharacterUImage];
    CGPoint loc = [sender locationInView:self.view];
    NSLog(@"sender value%@",sender);
    tmpCharacterUIIV.center = loc;
    
    //Paste Character to view and enable user interaction
    [self.characterImageView addSubview:tmpCharacterUIIV];

    
     //Delete character
     UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(deleteCharacter:)];
     doubleTap.delegate = self;
     doubleTap.numberOfTapsRequired = 2;
     [tmpCharacterUIIV addGestureRecognizer:doubleTap];
     [singleTap requireGestureRecognizerToFail:doubleTap];
     
     //Handle pan
     UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
     pan.delegate = self;
     [tmpCharacterUIIV addGestureRecognizer:pan];
     [pan setMinimumNumberOfTouches:1];
     [pan setMaximumNumberOfTouches:1];
     
     //Handle pinch
     UIPinchGestureRecognizer *pinch = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinch:)];
     pinch.delegate = self;
     [tmpCharacterUIIV addGestureRecognizer:pinch];
     
     //Handle Rotation
     UIRotationGestureRecognizer *rotation = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(handleRotation:)];
     rotation.delegate = self;
     [tmpCharacterUIIV addGestureRecognizer:rotation];
    
    tmpCharacterUIIV.userInteractionEnabled = YES;
}
-(void)deleteCharacter:(UITapGestureRecognizer *)recognizer
{
    [recognizer.self.view removeFromSuperview];
}
- (IBAction)clearScreen:(id)sender
{
    for (int i = [characterImageView.subviews count] -1; i>=0; i--)
    {
        if ([[characterImageView.subviews objectAtIndex:i] isKindOfClass:[UIImageView class]])
        {
            [[characterImageView.subviews objectAtIndex:i] removeFromSuperview];
        }
    
    }
    
    frameImageView.hidden = YES;
    [DataController resetSharedInstance];
}



- (void)handlePan:(UIPanGestureRecognizer *)recognizer
{
    CGPoint translation = [recognizer translationInView:self.view];
    recognizer.view.center = CGPointMake(recognizer.view.center.x + translation.x,
                                         recognizer.view.center.y + translation.y);
    [recognizer setTranslation:CGPointMake(0, 0) inView:self.view];
}
-(void)handlePinch:(UIPinchGestureRecognizer *)recognizer
{
    if([recognizer state] == UIGestureRecognizerStateBegan) {
        // Reset the last scale, necessary if there are multiple objects with different scales
        lastScale = [recognizer scale];
    }
    
    if ([recognizer state] == UIGestureRecognizerStateBegan ||
        [recognizer state] == UIGestureRecognizerStateChanged)
    {
        CGFloat currentScale = [[[recognizer view].layer valueForKeyPath:@"transform.scale"] floatValue];
        // Constants to adjust the max/min values of zoom
        const CGFloat kMaxScale = 1.7;
        const CGFloat kMinScale = 0.7;
        // new scale is in the range (0-1)
        CGFloat newScale = 1 -  (lastScale - [recognizer scale]);
        newScale = MIN(newScale, kMaxScale / currentScale);
        newScale = MAX(newScale, kMinScale / currentScale);
        CGAffineTransform transform = CGAffineTransformScale([[recognizer view] transform], newScale, newScale);
        [recognizer view].transform = transform;
        // Store the previous scale factor for the next pinch gesture call
        lastScale = [recognizer scale];
    }
    
}
-(void)handleRotation:(UIRotationGestureRecognizer *)recognizer
{
    UIGestureRecognizerState state = recognizer.state;
    
    if (state == UIGestureRecognizerStateBegan || state == UIGestureRecognizerStateChanged)
    {
        CGFloat rotation = [recognizer rotation];
        [recognizer.view setTransform:CGAffineTransformRotate(recognizer.view.transform, rotation)];
        [recognizer setRotation:0];
    }
}

- (void)viewDidLoad
{
   // singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pasteImage:)];
    //[self.previewImage addGestureRecognizer:singleTap];
    [super viewDidLoad];
    
   

}

//Set viewWillAppear to BOOL
- (void)viewWillAppear:(BOOL)animated

{
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIColor whiteColor],NSForegroundColorAttributeName,
                                    [UIColor whiteColor],NSBackgroundColorAttributeName,nil];
    
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;
    self.title = @"Preview";
    [self.view setBackgroundColor:[UIColor colorWithRed:207.0/255.0
                                                  green:16.0/255.0
                                                   blue:13.0/255.0
                                                  alpha:1.0]];
 
    UIBarButtonItem *shareButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(showActionSheet:)];
    NSArray *actionButtonItems = @[shareButton];
    self.navigationItem.rightBarButtonItems = actionButtonItems;
    
    [super viewWillAppear:animated];
   // frameImageView.hidden = NO;
}

-(void) viewWillDisappear:(BOOL)animated
{
    
  //  frameImageView.hidden = YES;
}
@synthesize previewImage;


- (void)viewDidUnload
{
    [self setPreviewImage:nil];
    [super viewDidUnload];
}

//Load still image without frame
- (void)didSelectStillImage:(NSData *)imageData withError:(NSError *)error
{
    if(!error)
    {
        UIImage *image = [[UIImage alloc] initWithData:imageData];
        previewImage.image = image;
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Capture Error" message:@"Unable to capture photo." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }

}
//Load still image with frame
- (void)didSelectStillImageWithFrame:(NSData *)imageData withError:(NSError *)error
{
    //[self loadFrame];
    if(!error)
    {
        UIImage *image = [[UIImage alloc] initWithData:imageData];
        previewImage.image = image;
        [self loadFrame];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Capture Error" message:@"Unable to capture photo." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
    
}

- (void)panCharacter:(UIPanGestureRecognizer *)recognizer
{
    NSLog(@"Pan Character");  
    CGPoint translation = [recognizer translationInView:self.view];
    recognizer.view.center = CGPointMake(recognizer.view.center.x + translation.x,
                                         recognizer.view.center.y + translation.y);
    [recognizer setTranslation:CGPointMake(0, 0) inView:self.view];
}
//Load frame from sharedInstance in DataController when comming from StacheCamViewController
-(void)loadFrame;
{
    DataController *dataController = [DataController sharedInstance];
    UIImage *tmp = [UIImage imageNamed:dataController.result];
    frameImageView.image = tmp;
    frameImageView.hidden = NO;

}


- (void)characterControllerLoad:(CharacterViewController *)sender
{
    
   
    
    
    tmpCharacterUImage = [UIImage imageNamed:sender.selectedLabelStr];
    tmpCharacterUIIV = [[UIImageView alloc]initWithImage:tmpCharacterUImage];
   // CGPoint loc = [ locationInView:self.view];
    tmpCharacterUIIV.center = self.characterImageView.center;
    
    singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pasteImage:)];
    [self.characterImageView addGestureRecognizer:singleTap];
    
    
    //Paste Character to view and enable user interaction
    [self.characterImageView addSubview:tmpCharacterUIIV];
    
    
    //Delete character
    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(deleteCharacter:)];
    doubleTap.delegate = self;
    doubleTap.numberOfTapsRequired = 2;
    [tmpCharacterUIIV addGestureRecognizer:doubleTap];
    [singleTap requireGestureRecognizerToFail:doubleTap];
    
    //Handle pan
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    pan.delegate = self;
    [tmpCharacterUIIV addGestureRecognizer:pan];
    [pan setMinimumNumberOfTouches:1];
    [pan setMaximumNumberOfTouches:1];
    
    //Handle pinch
    UIPinchGestureRecognizer *pinch = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinch:)];
    pinch.delegate = self;
    [tmpCharacterUIIV addGestureRecognizer:pinch];
    
    //Handle Rotation
    UIRotationGestureRecognizer *rotation = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(handleRotation:)];
    rotation.delegate = self;
    [tmpCharacterUIIV addGestureRecognizer:rotation];
    
    tmpCharacterUIIV.userInteractionEnabled = YES;
    
    [self.characterImageView addSubview:tmpCharacterUIIV];
    
    self.characterImageView.userInteractionEnabled = YES;
}
//Load frame after coming from the Template Controller
- (void)templateControllerLoadFrame:(TemplateController *)sender
{
    DataController *dataController = [DataController sharedInstance];
    UIImage *tmp = [UIImage imageNamed:dataController.result];
    frameImageView.image = tmp;
    frameImageView.hidden = NO;
    [self loadFrame];
}



// Push TemplateControler view
- (IBAction)showFrameSelector:(id)sender
{
    UIStoryboard *storyboard;
    if ([UIScreen mainScreen].bounds.size.height >= 568)
    {
        
        storyboard = [UIStoryboard storyboardWithName:@"iphone5" bundle:nil];
        TemplateController *templateController = [storyboard instantiateViewControllerWithIdentifier:@"TemplateController"];
        templateController.frameDelegate = self;
        [self presentViewController:templateController animated:YES completion:nil];

    }
    else
        storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil];
        TemplateController *templateController = [storyboard instantiateViewControllerWithIdentifier:@"TemplateController"];
        templateController.frameDelegate = self;
        [self presentViewController:templateController animated:YES completion:nil];
}

// Push TemplateControler view
- (IBAction)showCharacterSelector:(id)sender
{
    UIStoryboard *storyboard;
    if ([UIScreen mainScreen].bounds.size.height >= 568)
    {
        
        storyboard = [UIStoryboard storyboardWithName:@"iphone5" bundle:nil];
        CharacterViewController *characterController = [storyboard instantiateViewControllerWithIdentifier:@"CharacterViewController"];
        characterController.characterDelegate = self;
        [self presentViewController:characterController animated:YES completion:nil];
        
    }
    else

    
    storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil];
    CharacterViewController *characterController = [storyboard instantiateViewControllerWithIdentifier:@"CharacterViewController"];
    characterController.characterDelegate = self;
    [self presentViewController:characterController animated:YES completion:nil];
}

- (IBAction)showActionSheet:(id)sender
{
    NSString *actionSheetTitle = @"Save & Share to SNS"; //Action Sheet Title
    //NSString *destructiveTitle = @"Destructive Button"; //Action Sheet Button Titles
    NSString *facebook = @"Facebook";
    NSString *twitter = @"Twitter";
    NSString *saveToCamera = @"Save to Camera Roll";
    NSString *cancelTitle = @"Cancel";
    UIActionSheet *actionSheet =
    [[UIActionSheet alloc]initWithTitle:actionSheetTitle delegate:self cancelButtonTitle:cancelTitle
                 destructiveButtonTitle:nil otherButtonTitles:facebook, twitter, saveToCamera, nil];
    [actionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    

    //Get the name of the current pressed button
    NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    
    if ([buttonTitle isEqualToString:@"Facebook"])
    {
        [self postToFacebook];
        NSLog(@"Facebook button pressed");
    }
    if ([buttonTitle isEqualToString:@"Twitter"])
    {
        [self postToTwitter];
        NSLog(@"Twitter button pressed");
    }
    if ([buttonTitle isEqualToString:@"Save to Camera Roll"])
    {
        [self savePreview];
        NSLog(@"Save to camera roll pressed");
    }
    if ([buttonTitle isEqualToString:@"Cancel Button"])
    {
        NSLog(@"Cancel pressed --> Close ActionSheet");
    }
}
-(void)postToTwitter
{
    [SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter];
    SLComposeViewController *tweetController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    [tweetController setInitialText:@"First post from my iPhone app"];
    [self presentViewController:tweetController animated:YES completion:Nil];
    //set bounds size to get the contents on screen
    UIGraphicsBeginImageContext(self.view.bounds.size);
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    CGRect rect;
    rect = CGRectMake(0, 0, self.previewView.bounds.size.width, self.previewView.bounds.size.height);
    CGImageRef imageRef = CGImageCreateWithImageInRect([mergedImage CGImage], rect);
    NSLog(@"imageRef value%@", mergedImage);
    [tweetController addImage:[UIImage imageWithCGImage:imageRef]];
    CGImageRelease(imageRef);
    [self presentViewController:tweetController animated:YES completion:Nil];
}


-(void)postToFacebook
{
    [SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook];
    SLComposeViewController *controller =
        [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    [controller setInitialText:@"First post from my iPhone app"];
    [self presentViewController:controller animated:YES completion:Nil];
    
    CGRect rect;
    rect = CGRectMake(0, 0, self.previewView.bounds.size.width, self.previewView.bounds.size.height);
    CGImageRef imageRef = CGImageCreateWithImageInRect([mergedImage CGImage], rect);
    
    NSLog(@"imageRef value%@", mergedImage);
    [controller addImage:[UIImage imageWithCGImage:imageRef]];
    CGImageRelease(imageRef);
    [self presentViewController:controller animated:YES completion:Nil];
}

//Save all contents on the picture screen(UIView)
- (void)savePreview
{
    UIGraphicsBeginImageContext(self.previewView.bounds.size);
    [self.previewView.layer renderInContext:UIGraphicsGetCurrentContext()];
    mergedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIImageWriteToSavedPhotosAlbum(mergedImage, self, @selector(savingImageIsFinished:didFinishSavingWithError:contextInfo:), nil);
    UIGraphicsEndImageContext();
    
}
- (void) savingImageIsFinished:(UIImage *)_image didFinishSavingWithError:(NSError *)_error contextInfo:(void *)_contextInfo
{
	NSString* title = nil;
	NSString* message = nil;
    if(_error){//エラーのとき
		
		title = @"Error";
		message = @"Picture not saved, check your phone settings";
		
    }else{//保存できたとき
		message = @"Picture Saved";
    }
	UIAlertView *alert  = [[UIAlertView alloc] initWithTitle:title
									   message:message
									  delegate:nil
							 cancelButtonTitle:nil
							 otherButtonTitles:@"OK", nil
			 ];
	
	[alert show];
}

@end
