//
//  TestController.h
//  FramesCam
//
//  Created by Eduardo on 11/21/13.
//
//

#import <UIKit/UIKit.h>
#import "TemplateController.h"

@interface TestController : UIViewController
@property (retain, nonatomic) IBOutlet UIImageView *frameImageView;

@property(retain,nonatomic) NSString *passFrameLabel;
@property(retain,nonatomic) IBOutlet UIImageView *passFrameImage;
@end
