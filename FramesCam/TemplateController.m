//
//  TemplateController.m
//  FramesCam
//
//  Created by Eduardo on 11/18/13.
//
//

#import "PreviewController.h"
#import "TemplateViewCell.h"
#import "DataController.h"



@interface TemplateController ()<UITextFieldDelegate, UICollectionViewDelegateFlowLayout>


@end

@implementation TemplateController
@synthesize frameImages;

- (void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.topItem.title = @"";
}

- (void)viewDidLoad
    {  
        [super viewDidLoad];
        // Initialize image array
        NSArray *framesArray = [NSArray arrayWithObjects: @"none.png", @"Frame9.png", @"Frame8.png", @"Frame7.png", @"Frame6.png", @"Frame5.png", @"Frame4.png", @"Frame3.png", @"Frame2.png",@"Frame1.png", nil];
        self.frameImages = [NSArray arrayWithObjects: framesArray, nil];
        UICollectionViewFlowLayout *collectionViewLayout = (UICollectionViewFlowLayout*)self.collectionView.collectionViewLayout;
        collectionViewLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
        _selectedFrames = [NSMutableArray array];
        [super viewDidLoad];
    }

- (void)didReceiveMemoryWarning
    {
        [super didReceiveMemoryWarning];
        //Dispose of any resources that can be recreated.
    }

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
    {
        return [self.frameImages count];
    }

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
    {
        return [[self.frameImages objectAtIndex:section] count];
    }


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
    {
        static NSString *identifier = @"Cell";
        TemplateViewCell *cell = (TemplateViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        TemplateViewCell *frameImageViewCell = (TemplateViewCell *)[cell viewWithTag:100];
        frameImageViewCell.frameImageView.image = [UIImage imageNamed:[self.frameImages[indexPath.section] objectAtIndex:indexPath.row]];

      cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"test.png"]];
       
        return cell;
    }

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
    {
        [DataController sharedInstance];
        _selectedLabelStr = [self.frameImages[indexPath.section] objectAtIndex:indexPath.row];
        DataController *myStore = [DataController sharedInstance];
        myStore.result = _selectedLabelStr;
        [self dismissViewControllerAnimated:YES completion:
         ^{
        if ([self.frameDelegate respondsToSelector:@selector(templateControllerLoadFrame:)])
            {
                
                [self.frameDelegate performSelector:@selector(templateControllerLoadFrame:) withObject:self];
            }
         }];
    }



- (IBAction)back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
     
//     
//     ^{
//         if ([self.frameDelegate respondsToSelector:@selector(viewWillAppear:)])
//    {
//        
//        [self.frameDelegate performSelector:@selector(viewWillAppear:) withObject:self];
//    }
//     }];


}
@end
