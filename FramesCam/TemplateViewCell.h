//
//  TemplateViewCell.h
//  FramesCam
//
//  Created by Eduardo on 11/19/13.
//
//

#import <UIKit/UIKit.h>

@interface TemplateViewCell : UICollectionViewCell
@property (nonatomic, weak) IBOutlet UIImageView *frameImageView;

@end
