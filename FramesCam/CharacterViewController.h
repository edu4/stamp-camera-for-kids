//
//  CharacterControllerViewController.h
//  FramesCam
//
//  Created by Eduardo on 1/22/14.
//
//

#import <UIKit/UIKit.h>
@protocol CharacterControllerDelegate;
@interface CharacterViewController : UIViewController

//Making public CharacterControllerDelegate
@property (nonatomic, weak)id <CharacterControllerDelegate> characterDelegate;
@property(nonatomic, weak)   NSMutableArray *selectedCharacter;
@property(nonatomic, weak)  NSString *selectedLabelStr;
@property(nonatomic, strong) NSArray *characterImages;//cannot use weak
@property(nonatomic, weak)  IBOutlet UICollectionView *collectionView;


- (IBAction)back:(id)sender;
@end

@protocol CharacterControllerDelegate <NSObject>

- (void)characterControllerLoad:(CharacterViewController *)sender;

@end
