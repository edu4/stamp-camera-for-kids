//
//  Created by Eduardo on 11/26/13.
#import "DataController.h"


@implementation DataController
@synthesize result;

static DataController *sharedInstance = nil;
+(DataController*)sharedInstance
    {
        //Declare static variable to hold the instance of your class, ensuring it’s available globally inside your class.
        
        //static dispatch_once_t oncePredicate;
        //dispatch_once_t which ensures that the initialization code executes only once.
       // dispatch_once(&oncePredicate,
            {
                if (sharedInstance == nil)
                {
                    sharedInstance = [[DataController alloc]init];
                }
            }
        return sharedInstance;
    }

+ (void)resetSharedInstance {
    sharedInstance = nil;
}


@end
